<?php

/**
 * @file
 * Wraps the Menu Block plugin to display menu items in their rendered form.
 */

$plugin = array(
  'title' => t('Rendered menu tree'),
  'admin title' => 'menu_block_menu_tree_content_type_admin_title',
  'edit form' => 'menu_block_menu_tree_content_type_edit_form',
);

/**
 * Implements hook_PLUGIN_content_type_content_types().
 */
function rendered_menu_rendered_menu_content_type_content_types() {
  ctools_get_content_type('menu_tree');
  $items = menu_block_menu_tree_content_type_content_types();
  foreach ($items as &$item) {
    $item['defaults']['rendered_menu'] = array(
      'view_display' => 'page',
      'view_mode' => 'teaser',
    );
    $item['title'] = t('Rendered') . ': ' . $item['title'];
  }
  return $items;
}

/**
 * Implements hook_PLUGIN_content_type_render().
 */
function rendered_menu_rendered_menu_content_type_render($subtype, $conf, $args, $context) {
  $menu_conf = $conf['rendered_menu'];
  $context = array('subtype' => $subtype, 'conf' => $conf, 'args' => $args, 'context' => $context);
  drupal_alter('rendered_menu_conf', $menu_conf, $context);
  $view_mode = $menu_conf['view_mode'];
  $view_display = $menu_conf['view_display'];

  // Store the menu output separately and then clear out the content.
  $output = menu_block_menu_tree_content_type_render($subtype, $conf, $args, $context);
  $menu = $output->content['#content'];
  $output->content = array();

  // Loop through each element, and if it is an entity, render it.
  foreach (element_children($menu) as $mlid) {
    $item = menu_get_item($menu[$mlid]['#original_link']['link_path']);
    // Check for entities.
    if (isset($item['map'][1]) && is_object($item['map'][1])) {
      if ($entity = entity_view($item['map'][0], array($item['map'][1]), $view_mode)) {
        $output->content[] = $entity;
      }
    }
    // Check for views.
    elseif ($item['page_callback'] == 'views_page' && $view = views_get_view($item['page_arguments'][0])) {
      if ($view->access($view_display)) {
        $view->set_display($view_display);
        $output->content[]['#markup'] = $view->preview($view_display);
      }
    }
  }
  return $output;
}

/**
 * Implements hook_PLUGIN_content_type_admin_info().
 */
function rendered_menu_rendered_menu_content_type_admin_info($subtype, $conf, $context = NULL) {
  $output = menu_block_menu_tree_content_type_admin_info($subtype, $conf, $context);
  $output->title = t('Rendered') . ': ' . $output->title;
  return $output;
}
