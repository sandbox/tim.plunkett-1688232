<?php

/**
 * @file
 * Hooks provided by the Rendered Menu module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Override the view mode or view display to be used for a rendered menu.
 *
 * @param array $conf
 *   An associative array containing the following key-value pairs:
 *   - view_mode: The view mode to use to display entities.
 *   - view_display: The name of the display to use for a view.
 * @param array $context
 *   An associative array containing the following key-value pairs:
 *   - subtype: The name of the subtype being rendered.
 *   - conf: The stored configuration for the content.
 *   - args: Any arguments passed.
 *   - context: An array of contexts requested by the required contexts and
 *     assigned by the configuration step.
 */
function hook_rendered_menu_conf_alter(&$conf, $context) {
  $conf['view_mode'] = 'teaser';
  $conf['view_display'] = 'panel_page_1';
}

/**
 * @} End of "addtogroup hooks".
 */
